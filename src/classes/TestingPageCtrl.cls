public with sharing class TestingPageCtrl {

	private final sObject mysObject;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public TestingPageCtrl(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
    }

}